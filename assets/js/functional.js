document.addEventListener('DOMContentLoaded',function(){
	var mini_nav = document.querySelector('.mini-nav'),
	header = document.querySelector('.header'),
	body = document.querySelector('.body'),
	top = document.getElementById('top'),
	last_known_scroll_position = 0,
	ticking = false,
	plus = document.querySelector('.circle'),
	items = document.querySelector('.plus-items'),
	items_are_visible = '';

	var fct_array = {
		blind_click: function(clicked_mailto) {
			var new_href = clicked_mailto.href.replace(/AT/,'@').replace(/DOT/,'.');
			clicked_mailto.href = new_href;
		},
		switch_page: function(e, mini_link) {
			var target_class = '.' + e.target.name,
			requested_page = document.querySelector(target_class),
			current_page = document.querySelector('.current-page'),
			active_page = document.querySelector('.page.active');
			active_page.classList.remove('active');
			requested_page.classList.add('active');
			if (mini_link) {
				current_page.classList.remove('current-page');
				e.target.classList.add('current-page');
			} else {
				var target_name = e.target.name,
				mini_link_group = document.querySelectorAll('.mini-link');

				for (var i=0; i<mini_link_group.length; i++) {
					mini_link_group[i].classList.remove('current-page');
					if (mini_link_group[i].name.match(target_name)) {
						mini_link_group[i].classList.add('current-page');
					}
				}
				window.scrollTo(0, 0);
			}
		},
		be_sticky: function(scroll_pos) {
			var sticky_nav_present = document.querySelector('.mini-nav.sticky');
			if (sticky_nav_present && scroll_pos > header.offsetHeight) {
				return;
			} else if (sticky_nav_present && scroll_pos < header.offsetHeight) {
				mini_nav.classList.remove('sticky');
				header.appendChild(mini_nav);
				// reset items
				plus.style.display = 'none';
				plus.classList.remove('animate');
				items.style.display = 'none';
				items.classList.remove('animate');
				items_are_visible = false;
			} else if (!sticky_nav_present && scroll_pos > header.offsetHeight) {
				mini_nav.classList.add('sticky');
				plus.style.display = 'inline-block';
				body.insertBefore(mini_nav, top);
			}
		},
		show_more: function (items) {
			if (items_are_visible) {
				plus.classList.remove('animate');
				items.style.display = 'none';
				items.classList.remove('animate');
				items_are_visible = false;
			} else {
				plus.classList.add('animate');
				items.style.display = 'inline-block';
				items.classList.add('animate');
				items_are_visible = true;
			}
		}
	};

	//contact mail handler
	body.addEventListener('click', function(e) {
		var mini_link = '';
		if (e.target && e.target.classList.contains('mini-link')) {
			mini_link = true;
			fct_array.switch_page(e, mini_link);
		} else if (e.target && e.target.classList.contains('internal-link') || e.target && e.target.classList.contains('plus-link')) {
			mini_link = false;
			fct_array.switch_page(e, mini_link);
		} else if (e.target && e.target.classList.contains('mailto')) {
			var clicked_mailto = e.target;
			fct_array.blind_click(clicked_mailto);
		}
	});

	//scroll handler - passing sticky nav
	window.addEventListener('scroll', function(e) {
	  last_known_scroll_position = window.scrollY;
	  if (!ticking) {
	    window.requestAnimationFrame(function() {
	      fct_array.be_sticky(last_known_scroll_position);
	      ticking = false;
	    });
	  }
	  ticking = true;
	});

	//plus show more handler
	plus.addEventListener('click', function (e) {
		fct_array.show_more(items);
	});

});
